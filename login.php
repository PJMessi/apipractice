<?php 
require_once "config.php";
$loginURL = $gClient->createAuthUrl();
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
</head>
<body>
    <div class="container" style="margin_top: 100px">
        <div class="row justify-content-centre">
            <div class="col-md-6-offset-3" align="center">
                <img src="images/logo.png" alt=""><br><br>
                <form action="">
                    <input type="email" placeholder="Email...." class="form-control", name="email"><br>
                    <input type="password" name="password" placeholder="Password...." class="form-control"><br>
                
                        <input type="submit" class="form-control btn btn-primary" value="Signup">
                        <input type="button" onClick="window.location = '<?php echo $loginURL ?>';" class="form-control btn btn-danger" value="Signup with google">
                
                
                </form>
            </div>
        </div>
    </div>
</body>
</html>

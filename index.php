
<?php 
    require_once "config.php";
    $loginURL = $gClient->createAuthUrl();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>l</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>
</head>
<body>
    <form>
        <input placeholder="email" name="email">
        <input type="password" placeholder="password">
        <input type="submit" value="login">
        <input type="button" onclick="window.location = '<?php echo $loginURL ?>';" value="login with google">
    </form>
</body>
</html>